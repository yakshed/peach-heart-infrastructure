[
  {
    "name": "peach-heart-migrate",
    "image": "registry.gitlab.com/yakshed/peach-heart:master",
    "cpu": 1,
    "memory": 128,
    "essential": false,
    "command": ["./bin/rails", "db:migrate"],
    "environment": [
      {
        "name": "DATABASE_URL",
        "value": "postgres://${db_user}:${db_pass}@${db_address}/${db_name}"
      },
      {
        "name": "RAILS_ENV",
        "value": "production"
      },
      {
        "name": "RAILS_LOG_TO_STDOUT",
        "value": "true"
      },
      {
        "name": "SECRET_KEY_BASE",
        "value": "${rails_secret_key_base}"
      }
    ]
  },
  {
    "name": "peach-heart",
    "image": "registry.gitlab.com/yakshed/peach-heart:master",
    "cpu": 1,
    "memory": 512,
    "essential": true,
    "environment": [
      {
        "name": "DATABASE_URL",
        "value": "postgres://${db_user}:${db_pass}@${db_address}/${db_name}"
      },
      {
        "name": "RAILS_ENV",
        "value": "production"
      },
      {
        "name": "RAILS_LOG_TO_STDOUT",
        "value": "true"
      },
      {
        "name": "SECRET_KEY_BASE",
        "value": "${rails_secret_key_base}"
      }
    ],
    "portMappings": [
      {
        "containerPort": 3000,
        "hostPort": 3000
      }
    ]
  }
]
