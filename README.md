# Peach Heart Infrastructure

Creates an ECS cluster on AWS. This was intended for the [peach heart example app](https://gitlab.com/yakshed/peach-heart/) but should be generic enough for other apps as well.

It will create

* A RDS instance running Postgres 9.6.1
* 2 EC2 instances for the cluster
* A task definition to run
 * the peach heart Rails app
 * the peach heart migrations

Everything will run in your default VPC and no security groups are managed.

## Setup

You need an AWS account. Running this will eventually generate some costs although all selected instances are applicable for the free tier.

You need to fill in all the variables in `terraform.tfvars` and put your public SSH key in the project directory named `peach-heart-key.pub`.

## Todo

* Create dedicated VPC
* Create security groups with proper inbound rules
* Add auto scaling
* Add load balancer

## Caveat

At the moment we didn't figured how to deploy from Gitlab ¯\_(ツ)_/¯
