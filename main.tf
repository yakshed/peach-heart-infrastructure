provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region = "${var.aws_region}"
}

resource "aws_db_instance" "peach_heart" {
  identifier = "peach-heart-db"
  allocated_storage = 5
  engine = "postgres"
  engine_version = "9.6.1"
  instance_class = "db.t2.micro"
  name = "peach_heart"
  username = "postgres"
  password = "${var.db_password}"
}

data "template_file" "peach_heart_service" {
  template = "${file("task-definitions/peach-heart-service.json.tpl")}"

  vars {
    db_user = "${aws_db_instance.peach_heart.username}"
    db_pass = "${var.db_password}"
    db_address = "${aws_db_instance.peach_heart.address}"
    db_name = "${aws_db_instance.peach_heart.name}"
    rails_secret_key_base = "${var.rails_secret_key_base}"
  }
}

resource "aws_iam_role" "peach_heart" {
  name = "peach-heart-ecs"
  assume_role_policy = "${file("iam/peach-heart-role.json")}"
}

resource "aws_iam_role_policy" "peach_heart" {
  name = "peach-heart-ecs-instance-policy"
  role = "${aws_iam_role.peach_heart.id}"
  policy = "${file("iam/peach-heart-policy.json")}"
}

resource "aws_iam_instance_profile" "peach_heart" {
  name = "peach-heart-ecs-instance-profile"
  roles = ["${aws_iam_role.peach_heart.name}"]
}

resource "aws_instance" "peach_heart" {
  ami = "ami-e012d48f"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.peach_heart.key_name}"
  count = 2
  user_data = "${data.template_file.peach_heart_user_data.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.peach_heart.name}"
}

resource "aws_key_pair" "peach_heart" {
  key_name = "peach-heart-key"
  public_key = "${file("peach-heart-key.pub")}"
}

data "template_file" "peach_heart_user_data" {
  template = "${file("user-data/peach-heart-user-data.tpl")}"

  vars {
    ecs_cluster = "${aws_ecs_cluster.peach_heart.name}"
  }
}

resource "aws_ecs_cluster" "peach_heart" {
  name = "peach-heart"
}

resource "aws_ecs_task_definition" "peach_heart_service" {
  family = "peach-heart-service"
  container_definitions = "${data.template_file.peach_heart_service.rendered}"
}

resource "aws_ecs_service" "peach_heart" {
  name = "peach-heart"
  cluster = "${aws_ecs_cluster.peach_heart.id}"
  task_definition = "${aws_ecs_task_definition.peach_heart_service.arn}"
  desired_count = 1
}

// Variables

variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "aws_region" {}
variable "db_password" {}
variable "rails_secret_key_base" {}

// Output

output "ecs_cluster" {
  value = "${aws_ecs_cluster.peach_heart.id}"
}
